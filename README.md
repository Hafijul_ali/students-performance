# Students Performance
A linear regressor made using Python3 and scikit-learn library to derive a relationship between `number of hours` and `score` of a student


## Installation
To run the project in your local system

fist clone the repo,  using 
```
git clone https://gitlab.com/Hafijul_ali/students-performance.git
```
After cloning change directory into the project root, using

```
cd students-performance
```

Install dependencies using

```
pip install -r requirements.txt
```
or 

```
python -m pip install -r requirements.txt
```

**NOTE** Depending upon your system , `python` maynot work, in such a case use `python3` 


## Usage
After installing all required dependencies, run the program using

```
python main.py
```

The program takes one input of `float` type which denotes the `number of hours of study` done by a student. Regressor then prints the predicted marks of the student.


## Contributing
Contributions can be either code or non-code. All open source contributions are highly encouraged. 


## License
The project is licensed under MIT License


## Project status
Development of the project has been halted and `main` branch contains the most stable version of the code.

