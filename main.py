import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error

url = "http://bit.ly/w-data"


def fetch_data(url):
    return pd.read_csv(url)


def plot(data):
    data.plot(x='Hours', y='Scores', style='o')
    plt.title('Hours vs Percentage')
    plt.xlabel('Hours Studied')
    plt.ylabel('Percentage Score')
    plt.show(block=False)


def get_feature_target(data):
    return data.iloc[:, :-1].values, data.iloc[:, 1].values


def train_model(X_train, y_train):
    regressor = LinearRegression()
    regressor.fit(X_train, y_train)
    return regressor


def fit_line(regressor, X, y):
    line = regressor.coef_*X+regressor.intercept_
    plt.title('Hours vs Percentage')
    plt.xlabel('Hours Studied')
    plt.ylabel('Percentage Score')
    plt.scatter(X, y)
    plt.plot(X, line)
    plt.show(block=False)


def test_model(regressor, X_test):
    print("Test Set")
    print(X_test)
    y_pred = regressor.predict(X_test)
    return y_pred


def predict_percentage_score(regressor, hours):
    own_pred = regressor.predict(hours)
    print("No of Hours = {}".format(hours))
    print("Predicted Score = {}".format(own_pred[0]))


def model_metrics(y_test, y_pred):
    print('Mean Absolute Error : ', mean_absolute_error(y_test, y_pred))


def main():
    data = fetch_data(url)
    print("Data imported successfully")
    plot(data)
    print("Extracting features from dataset")
    X, y = get_feature_target(data)
    print("Splitting dataset into train and test batches")
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.3, random_state=0)
    print("Training model")
    regressor = train_model(X_train, y_train)
    print("Training complete")
    fit_line(regressor, X, y)
    print("Evaluating Model")
    y_predicted = test_model(regressor, X_test)
    hours = [[float(input("Enter number of hours (default 4.5h) : ") or "4.5")]] 
    predict_percentage_score(regressor, hours)
    print("Model statistics and standard metrics")
    model_metrics(y_test, y_predicted)


if __name__ == "__main__":
    main()
